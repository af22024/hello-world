import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class af22024 {
    private JPanel root;
    private JButton saladButton;
    private JButton chickenButton;
    private JButton doreaButton;
    private JButton pastaButton;
    private JButton pizzaButton;
    private JButton dessertButton;
    private JButton checkOutButton;
    private JTextArea orderlist;
    private JLabel totalprice;
    private int totalPrice = 0;

    private void handleOrder(String food, int foodprice) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+ food +" it will be served as soon as possible");
            addOrder(food, foodprice);
        }
    }

    private void addOrder(String food, int foodprice) {
        String currentText = orderlist.getText();
        orderlist.setText(currentText + food + " "  + foodprice +  "yen\n");
        totalPrice += foodprice;
        totalprice.setText("Total " + totalPrice + "yen");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("af22024");
        frame.setContentPane(new af22024().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void clearOrders() {
        orderlist.setText(""); // 注文リストをクリア
        totalPrice = 0; // 合計金額をリセット
        totalprice.setText("Total " + totalPrice + "yen");
    }

    public af22024() {
        saladButton.setIcon(new ImageIcon(this.getClass().getResource("salad.jpg")));
        chickenButton.setIcon(new ImageIcon(this.getClass().getResource("chicken.jpg")));
        doreaButton.setIcon(new ImageIcon(this.getClass().getResource("dorea.jpg")));
        pastaButton.setIcon(new ImageIcon(this.getClass().getResource("pasta.jpg")));
        pizzaButton.setIcon(new ImageIcon(this.getClass().getResource("pizza.jpg")));
        dessertButton.setIcon(new ImageIcon(this.getClass().getResource("dessert.jpg")));

            saladButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    handleOrder("salad", 350);
                }
            });
            chickenButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    handleOrder("chicken", 300);
                }
            });
            doreaButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    handleOrder("dorea", 430);
                }
            });
            pastaButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    handleOrder("pasta", 500);
                }
            });
            pizzaButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    handleOrder("pizza", 400);
                }
            });
            dessertButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    handleOrder("dessert", 250);
                }
            });
            checkOutButton.addActionListener(new ActionListener() {
                @Override
                public  void actionPerformed(ActionEvent e) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout? ",
                            "checkout",
                            JOptionPane.YES_NO_OPTION);

                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is "+totalPrice+" yen ");
                        clearOrders();
                    }
                }
            });
            }
    }





